FROM maven:3.8.1-jdk-11-slim
WORKDIR /app
COPY app/ .
RUN mvn spring-boot:build-image -f pom.xml && \
# RUN mvn package && \
    mv target/worker-jar-with-dependencies.jar /run/worker.jar
CMD ["java","-jar","/run/worker.jar"]