pipeline {
    agent {
        docker {
            image 'maven:3.8.1-jdk-11-slim'
            args '-v $HOME/.m2:/root/.m2'
        }
    }
    stages {
        stage('get code') {
            steps {
                dir('app') {
                    git branch: "main", url: 'https://github.com/iranzoferri/continuous-delivery-example.git'
                }
            }
        }
        stage('build') {
            steps {
                dir('app') {
                    echo 'building worker app'
                    sh 'mvn compile'
                }
            }
        }
        stage('test') {
            steps {
                echo 'running unit tests on worker app'
                dir('app') {
                    sh 'mvn clean test'
                }
            }
        }
        stage('package') {
            steps {
                echo 'packaging worker app into a jarfile'
                dir('app') {
                    sh 'mvn package -DskipTests'
                    archiveArtifacts artifacts: '**/target/*.jar', fingerprint: true
                }
            }
        }
    }
    post {
        always {
            echo 'the job is complete'
        }
    }
}
